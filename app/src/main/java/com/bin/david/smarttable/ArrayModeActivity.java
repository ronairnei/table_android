package com.bin.david.smarttable;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.bin.david.form.core.SmartTable;
import com.bin.david.form.core.TableConfig;
import com.bin.david.form.data.CellInfo;
import com.bin.david.form.data.column.Column;
import com.bin.david.form.data.format.draw.IDrawFormat;
import com.bin.david.form.data.style.FontStyle;
import com.bin.david.form.data.style.LineStyle;
import com.bin.david.form.data.table.ArrayTableData;
import com.bin.david.form.utils.DensityUtils;

public class ArrayModeActivity extends AppCompatActivity {

    private SmartTable<String> table;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_table);
        FontStyle.setDefaultTextSize(DensityUtils.sp2px(this, 15));
        table = (SmartTable<String>) findViewById(R.id.table);
        String[] week = {"ALUNO1", "ALUNO2", "ALUNO3","ALUNO4","ALUNO5", "ALUNO6" };
        String[][] infos = {
                {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"},
                {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"},
                {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"},
                {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"},
                {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"},
                {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"}};
        FontStyle fontStyle = new FontStyle(this, 10, ContextCompat.getColor(this, R.color.arc_text));
        table.getConfig().setColumnTitleStyle(fontStyle);
        table.getConfig().setHorizontalPadding(0);

        table.getConfig().setVerticalPadding(0);
        table.getConfig().setContentGridStyle(new LineStyle());
        table.getConfig().setFixedFirstColumn(true);

        final ArrayTableData<String> tableData = ArrayTableData.create("essa tabela", week, infos, new IDrawFormat<String>() {

            @Override
            public int measureWidth(Column<String> column, int position, TableConfig config) {
                return DensityUtils.dp2px(ArrayModeActivity.this, 50);
            }

            @Override
            public int measureHeight(Column<String> column, int position, TableConfig config) {
                return DensityUtils.dp2px(ArrayModeActivity.this, 50);
            }

            @Override
            public void draw(Canvas c, Rect rect, CellInfo<String> cellInfo, TableConfig config) {
                Paint paint = config.getPaint();
                int color;
                switch (cellInfo.data) {
                    case "a":
                        color = R.color.github_con_1;
                        break;
                    case "b":
                        color = R.color.github_con_2;
                        break;
                    case "c":
                        color = R.color.github_con_3;
                        break;
                    case "d":
                        color = R.color.github_con_4;
                        break;
                    default:
                        color = R.color.github_con_0;
                        break;
                }
//                paint.setStyle(Paint.Style.FILL);
                paint.setColor(ContextCompat.getColor(ArrayModeActivity.this, color));

//                paint.setColor(Color.BLACK);
                paint.setTextSize(20);

                c.drawText("oi", rect.exactCenterX(), rect.exactCenterY(), paint);
//                c.drawText("lalalalal", rect.left, rect.top , rect.right , rect.bottom, paint);
                // c.drawRect(rect.left + 5, rect.top + 5, rect.right - 5, rect.bottom - 5, paint);
            }
        });
//        tableData.setOnItemClickListener(new ArrayTableData.OnItemClickListener<Integer>() {
//            @Override
//            public void onClick(Column column, String value, Integer o, int col, int row) {
//                tableData.getArrayColumns().get(col).getDatas().get(row);
//                Toast.makeText(ArrayModeActivity.this, "列:" + col + " 行：" + row + "数据：" + value, Toast.LENGTH_SHORT).show();
//            }
//        });

        table.setTableData(tableData);
    }


}
